# Rebase Tour

1. Create the rebase branches:
   ```sh
   for suffix in main A B; do
       git branch demo/rebase/"${suffix}" demo/setup/"${suffixf}"
   done
   ```
1. As author a1, it is now time for you to combine your changes with the changes in the main branch. To do so, run:
   ```sh
   git checkout demo/rebase/A
   git rebase demo/rebase/main
   git checkout ${-1}
   git merge --ff-only demo/rebase/A
   ```
1. As author a2, you now want to get the latest updates from the main branch. You perform:

   ```bash
   git checkout demo/rebase/B
   git rebase demo/rebase/main
   ```

   You would also like to test your code now that it has been merged:

   ```bash
   source .venv/bin/activate
   python3 main.py
   ```

   However, you face errors with this:

   ```
   Traceback (most recent call last):
   File "/home/kylek/repos/git-falsification-demo/main.py", line 2, in <module>
       import angles
   File "/home/kylek/repos/git-falsification-demo/angles/__init__.py", line 1, in <module>
       from .angles import get_angle_type
   ImportError: cannot import name 'get_angle_type' from 'angles.angles' (/home/kylek/repos/git-falsification-demo/angles/angles.py)
   ```

   Despite the rebase being performed with no conflicts, breaking changes were introduced. This can happen regardless of the merge strategy used, but going back to a previously working state is now impossible.

   In fact, after fixing this problem, there is now a subtle logic error in the program: users can enter angles outside of the [0, 360) range without errors being generated!

   If, for instance, it is more important to get the `main.py` changes out into production than it is to have the new angle quadrant classifier, aside from using the reflog, it is impossible to revert to a working version of `main.py` using Git.

   Although the reflog can be used (`git reset @{1} --hard` would get author a2 back to the state of demo/setup/B before the rebase), it is an unacceptable solution because reflogs are not synchronized and dangling commits are garbage collected eventually. This also means that not contributors do not have the same reflog entries, so they are not equally able to revert to previous working versions of `main.py`.

# Merge Tour

1. Create the merge branches:
   ```sh
   for suffix in main A B; do
       git branch demo/merge/"${suffix}" demo/setup/"${suffixf}"
   done
   ```
1. As author a1, it is now time for you to combine your changes with the changes in the main branch. To do so, run:
    ```sh
    git checkout demo/merge/main
    git merge demo/merge/A --no-ff 
    ```
    Alternatively, author a1 could merge the latest changes from demo/merge/main into demo/merge/A in order to test the changes first. That would be done as follows:
    ```sh
    git checkout demo/merge/A
    git merge demo/merge/main --no-ff
    # Perform testing on the combined results, then when ready,
    git checkout @{-1}
    git merge @{-1} --no-ff
    ```
1. At this point, the output of `git log` can be compared with the output when the squashing strategy was used:
    // TODO add output

    Although the history is not as "pretty", it can easily be made identical with the addition of the `--first-parent` flag:
    ```sh
    git log demo/merge/main --first-parent
    ```
    So using the merge strategy, one is able to preserve all history while also being able to see a cleaner view of the history of any particular branch.

    Compared to the `git log` output when the rebase was performed, `git log --first-parent` is shorter and has a more idealized view of what has been put on the demo/merge/main branch, while `git log` shows the true graph granularity. Neither is the same as the linearity shown by `git log` with the rebase merge strategy, but this history is not in fact the true development history anyway.
1. As author a2, you now want to get the latest updates from the main branch. You perform:
    ```sh
    git pull origin demo/setup/main
    ```
    Similarly to prior cases, no conflicts were presented when the merge happened, yet the `main.py` module has two bugs: an import error, and allowing users to enter angles outside of [0, 360).

    However, in this case, the author can easily revert to the prior working state:
    ```sh
    git checkout HEAD^1
    ```
    At this point, `main.py` functions correctly again because the codebase is in the exact same state as it was prior to the merge. This did not rely on the reflog to retrieve history, and all other developers on the project are capable of performing this operation.

    If the `main.py` functionality were more important than the new changes introduced by author a1, then this commit could be published instead of the latest from main, or a1's merge could be reverted and re-applied later:
    ```sh
    git checkout demo/merge/main
    # Create a new commit that performs the inverse changes of a1's merge.
    git revert --mainline 1 HEAD
    ```

