readonly demo_prefix="${demo_prefix:-demo}" setup_prefix="${setup_prefix:-setup}" squash_prefix="${squash_prefix:-squash}" rebase_prefix="${rebase_prefix:-rebase}" merge_prefix="${merge_prefix:-merge}"

# Provides a convenience command to run git with a given author set.
# Usage: gita {author} [git commands...]
# First tries to determine the author's email address from a function `author_to_email`.
# If invoking this function returns a non-zero exit status, then `author` is used as the email address.
# Same goes with the author name, with a function called `author_to_name`
function gita() {(
    set -o pipefail -o errexit -o nounset
    function get_email() {
        local email="${1}"
        if 1>/dev/null 2>&1 type author_to_email; then
            email="$(author_to_email "${1}" || "${email}")"
        fi
        echo "${email}"
    }
    function get_name() {
        local name="${1}"
        if 1>/dev/null 2>&1 type author_to_name; then
            name="$(author_to_name "${1}" || "${name}")"
        fi
        echo "${name}"
    }
    
    local readonly email="$(get_email "${1}")"
    local readonly name="$(get_name "${1}")"
    shift 1
    # TOOD remove echo
    git -c user.signingkey="${email}" -c user.email="${email}" -c user.name="${name}" "$@"
)}

function cleanup() {
    set -o pipefail -o nounset
    for branch in "${demo_prefix}"/{"${squash_prefix}","${rebase_prefix}","${merge_prefix}"}/{main,A,B}; do
        git branch --delete --force "${branch}"
    done
}

function copy_setup_branches() {
    for prefix in rebase squash merge; do
        for postfix in main A B; do
            git branch "demo/${prefix}/${postfix}" "demo/setup/${postfix}"
        done
    done
}
